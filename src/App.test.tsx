import { render } from '@testing-library/react';
import React from 'react';
import App from './App';

test('renders title', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Calculator/i);
  expect(linkElement).toBeInTheDocument();
});
