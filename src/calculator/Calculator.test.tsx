import { render } from "@testing-library/react";
import React from "react";
import Calculator from "./Calculator";

test("renders all the buttons", async () => {
  const { findAllByRole } = render(<Calculator />);
  const buttons = await findAllByRole("button");
  expect(buttons.length).toEqual(16);
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  buttons.forEach((b) => expect(b).toBeInTheDocument());
});
