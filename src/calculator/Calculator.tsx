import { default as axios } from 'axios';
import React from 'react';
import './Calculator.scss';

enum Operator {
  ADDITION = '+',
  SUBTRACTION = '-',
  MULTIPLICATION = '*',
  DIVISION = '/',
  CLEAR = 'C',
  EQUALS = '=',
}

interface CalculatorState {
  firstNumberDigits: number[];
  secondNumberDigits: number[];
  operator?: Operator;
  displayValue: string;
}

const initialState: CalculatorState = {
  firstNumberDigits: [],
  secondNumberDigits: [],
  displayValue: '',
  operator: undefined,
};

class Calculator extends React.Component<unknown, CalculatorState> {
  constructor(props: CalculatorState) {
    super(props);
    this.state = initialState;
  }

  renderButton(symbol: Operator | number, className?: string): JSX.Element {
    return (
      <button className={className} onClick={() => this.clickOnButton(symbol)}>
        {symbol}
      </button>
    );
  }

  clickOnButton(symbol: Operator | number): void {
    switch (symbol) {
      case Operator.CLEAR:
        this.setState(initialState);
        break;
      case Operator.EQUALS:
        this.getResult();
        break;
      case Operator.ADDITION:
      case Operator.SUBTRACTION:
      case Operator.MULTIPLICATION:
      case Operator.DIVISION:
        if (this.state.operator || !this.state.firstNumberDigits.length) {
          break;
        }
        this.setState({ operator: symbol });
        this.addToDisplay(symbol);
        break;
      default:
        this.addToDisplay(symbol);
        if (!this.state.operator) {
          this.setState({ firstNumberDigits: [...this.state.firstNumberDigits, symbol] });
        } else {
          this.setState({ secondNumberDigits: [...this.state.secondNumberDigits, symbol] });
        }
    }
  }

  private addToDisplay(value: Operator | number): void {
    this.setState({ displayValue: `${this.state.displayValue}${value}` });
  }

  private getResult(): void {
    const anyDetailMissing = !this.state.firstNumberDigits.length || !this.state.secondNumberDigits.length || !this.state.operator;
    if (anyDetailMissing) {
      return;
    }
    axios
      .get<{result: number}>(`http://localhost:3001/${this.endpointNameFromOperator()}`, {
        params: {
          a: this.state.firstNumberDigits.join(''),
          b: this.state.secondNumberDigits.join(''),
        },
      })
      .then((res) => {
        this.setState({ ...initialState, firstNumberDigits: [res.data.result] });
        this.addToDisplay(res.data.result);
      })
      .catch(console.error);
  }

  private endpointNameFromOperator(): string {
    switch (this.state.operator) {
      case Operator.ADDITION:
        return 'addition';
      case Operator.SUBTRACTION:
        return 'subtraction';
      case Operator.MULTIPLICATION:
        return 'multiplication';
      case Operator.DIVISION:
        return 'division';
      default:
        return '';
    }
  }

  render(): JSX.Element {
    return (
      <div className="Container">
        <input readOnly type="text" className="Display" value={this.state.displayValue}></input>
        <div className="Row">
          <div className="Column">
            {this.renderButton(1)}
            {this.renderButton(4)}
            {this.renderButton(7)}
          </div>
          <div className="Column">
            {this.renderButton(2)}
            {this.renderButton(5)}
            {this.renderButton(8)}
            {this.renderButton(0)}
          </div>
          <div className="Column">
            {this.renderButton(3)}
            {this.renderButton(6)}
            {this.renderButton(9)}
          </div>
          <div className="Column">
            {this.renderButton(Operator.ADDITION, 'operator')}
            {this.renderButton(Operator.SUBTRACTION, 'operator')}
            {this.renderButton(Operator.MULTIPLICATION, 'operator')}
            {this.renderButton(Operator.DIVISION, 'operator')}
          </div>
          <div className="Column">
            {this.renderButton(Operator.CLEAR, 'c')}
            {this.renderButton(Operator.EQUALS, 'equals')}
          </div>
        </div>
      </div>
    );
  }
}

export default Calculator;
