import React from 'react';
import './App.css';
import Calculator from './calculator/Calculator';

function App(): JSX.Element {
  return (
    <div className="App">
      <h1>Calculator</h1>
      <Calculator />
    </div>
  );
}

export default App;
